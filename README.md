# Sitecore.Bin.Nugets #
Converting Sitecore binary libraries to the debuggable NuGets using JetBrains dotPeek


## Prerequisites: ##

1. Install JetBrains dotpeek decompiler ([https://www.jetbrains.com/decompiler/download/](https://www.jetbrains.com/decompiler/download/))
2. VS2015 with Powershell extension

## Why Sitecore Nugets? ##
1. No Sitecore binaries in the GIT or any source control solution. The Sitecore 8.0 has in bin folder ~190 files and size ~68mb. 
2. No different build Sitecore binary mismatch during upgrades. Just use VS Nuget package manager to upgrade  binaries.
3. Using JetBrains Resharpers 'Assembly Explorer' window easy to search any Sitecore Binary  asset and see source code file or method right away
4. The developer can easy make breakpoints, attach and step trough Sitecore code
5. It takes 10-20 min to generate nuget collection for any Sitecore build (The most time consuming & annoying operation which takes around 90% of time is dotPeek command to generate `*.pdb` files for each `*.dll`)

## Purpose of the project ##
Build the Nuget packages ~170 for each Sitecore 8.x.x. build. The each package should contain the *.dll and *.pdb file(s). This allow store Nugets on the private myget.org or companies Nuget server of choice (Team City, Artifactory [http://www.jfrog.com/open-source/](http://www.jfrog.com/open-source/ "Artifactory"), ProGet [https://inedo.com/proget/overview](https://inedo.com/proget/overview "ProGet") or local machine physical path. The Nuget solution can be used for every case and every budget.

## What is a PDB file? 
PDB is an abbreviation for **P** rogram **D a**ta **B** ase. As the name suggests it is a repository (persistant storage as databases) to maintain information required to run your program in debug mode. It contains many important relevant information required while debugging your code for e.g. at what points you have inserted break points where you expect the debugger to break in visual studio. This is the reason why many times Visual studio fails to hit the break points if you remove the *.pdb files from your debug folders. The visual studio debugger is also able to tell you the precise line number of code file at which an exception occurred in a stack trace with the help of *.pdb files.

## VS Debugging ##

If you stop now you should be able to attach to your process, step into the decompiled dll. But you will be not able to see the values of the variables. 
Two options to start visual studio in the correct mode:


- To start it once type the following lines into a cmd prompt:

		set COMPLUS_ZapDisable=1
		cd /d C:\Program Files (x86)\Microsoft Visual Studio 12.0\Common7\IDE
		start devenv.exe
		exit

- To set it permanently set this in the registry:

		HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Session Manager\Environment
		Add Key: COMPLUS_ZAPDISABLE
		Type: REG_SZ (String)
		Value: 1

You also need to disable the JIT optimization for the dll. To do that, a .ini file with the same name as the dll will be in added to NuGet package (copied bin folder during the build). Example for Sitecore.Kernel.dll, the name must be: Sitecore.Kernel.ini. Set the content of the file to:

		[.NET Framework Debugging Control] 
		GenerateTrackingInfo=1 
		AllowOptimize=0

Now you should be able to add the watch on the variables. 

Original blog [http://sitecoreblog.blogspot.com/](http://sitecoreblog.blogspot.com/ "sitecoreblog.blogspot.com")
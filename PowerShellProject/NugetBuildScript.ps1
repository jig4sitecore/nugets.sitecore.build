﻿# Authors: Jim Baltikauskas (jbaltikauskas@gmail.com)

If (-NOT ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator"))
{   
#"No Administrative rights, it will display a popup window asking user for Admin rights"

$arguments = "& '" + $myinvocation.mycommand.definition + "'"
Start-Process "$psHome\powershell.exe" -Verb runAs -ArgumentList $arguments

break
}
#"After user clicked Yes on the popup, your file will be reopened with Admin rights"

Add-Type –AssemblyName System.Xml
Add-Type –AssemblyName System.Xml.Linq

$scriptRoot = Split-Path (Resolve-Path $myInvocation.MyCommand.Path)

. "$scriptRoot\Common.ps1"

[string]$scriptRoot = Split-Path (Resolve-Path $myInvocation.MyCommand.Path) 
[string]$vsSolutionRoot = [System.IO.Path]::GetDirectoryName($scriptRoot) + '\'
[string]$sitecoreVersion = $config.Settings.SitecoreVersion
[string]$outputDirectory = $vsSolutionRoot + $config.Settings.NugetOutputDirectory

Function NugetPackCommand([string]$nuspecFile)
{
	[string] $cmd = $vsSolutionRoot + $config.Settings.NugetExe
	[string] $arg1 = '"' + $nuspecFile + '"' 
	[string] $arg2 = '"' + $outputDirectory + '"'

	if(-Not(Test-Path $outputDirectory))
	{
		Log-Info "Creating nuget output folder: $outputDirectory"
		[System.IO.Directory]::CreateDirectory($outputDirectory)
	}

	<# Execute command #>
	& $cmd pack $arg1 -OutputDirectory $arg2
}

Function UpdateNugetSpecVersion([string]$nuspecFile)
{
	[System.Xml.XmlDocument]$document = New-Object System.Xml.XmlDocument
	$document.Load($nuspecFile)
	$document.package.metadata.version = $sitecoreVersion
	$document.Save($nuspecFile)
}

Try
{
	Log-Info "Script execution directory:  $scriptRoot"

	# Add Common utility library
    . "$scriptRoot\Common.ps1"
	
	[string] $nuspecDirectory = $vsSolutionRoot + $config.Settings.NugetSpecDirectory
	Log-Info "Geting nuget spec files from directory: $nuspecDirectory"
	$files = [System.IO.Directory]::GetFiles($nuspecDirectory, "*.nuspec")
	

	ForEach($file in $files){
		Log-Info "Updating Sitecore Nuget spec file version tag to: $sitecoreVersion" 
		UpdateNugetSpecVersion $file

		Log-Info "Executing nuget pack command for file: $file"
		NugetPackCommand $file
	}

    Log-Info "Done. Move build artifacts (nugets) from $outputDirectory to destination!"
}
Catch
{
    $ErrorMessage = $_.Exception.Message
    Log-Error $ErrorMessage
}

pause
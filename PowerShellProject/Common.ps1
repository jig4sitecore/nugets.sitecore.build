Add-Type –AssemblyName System.Xml
Add-Type –AssemblyName System.Xml.Linq

$scriptRoot = Split-Path (Resolve-Path $myInvocation.MyCommand.Path)
$config = New-Object System.Xml.XmlDocument
$config.Load($scriptRoot + "\setup.config")


$infoLog =  $scriptRoot + "\info.log"
$errorLog = $scriptRoot + "\error.log"


Function Log-Info
{
    [CmdletBinding()]
    PARAM($message)

    begin
    {
        $tempPreference = $ErrorActionPreference
        $ErrorActionPreference = "continue"
    }

    process
    {
        Write-Host $message -ForegroundColor Green
        $message | Out-File $infoLog -Append
    }

    end
    {
        $ErrorActionPreference = $tempPreference
    }
}

Function Log-Error
{
    [CmdletBinding()]
    PARAM($message)

    begin
    {
        $tempPreference = $ErrorActionPreference
        $ErrorActionPreference = "continue"
    }

    process
    {
        Write-Host $message -ForegroundColor DarkRed
        $message | Out-File $errorLog -Append
    }

    end
    {
        $ErrorActionPreference = $tempPreference
    }
}


<#
    .DESCRIPTION
        Creates Folder if it doesn't exist
#>
Function Create-Folder
{
    [CmdletBinding()]
    PARAM($path)

    if((Test-Path $path) -eq 0)
    {
        mkdir $path
    }
}

<#
    .DESCRIPTION
        Creates Folder if it doesn't exist
#>
Function Git-Clone-Pull
{
    [CmdletBinding()]
    PARAM($gitUrl, $gitBranch, $gitPath)

    Log-Info "Begin - Git Clone/Pull $gitUrl $gitBranch $gitPath"
    Log-Info "        Git Url: $gitUrl"
    Log-Info "        Git Branch: $gitBranch"
    Log-Info "        Git Path: $gitPath"

    if((Test-Path $gitPath) -eq 0)
    {
        Log-Info "Executing command: git clone $gitUrl -b $gitBranch $gitPath"
        git clone $gitUrl -b $gitBranch $gitPath
    }
    else
    {
        cd "$gitPath"
        git reset --hard
        git clean -d -x -f
        git pull
    }

    Log-Info "End - Git Clone/Pull"
}

<#
    .DESCRIPTION
        Set ACL rules for folder
#>
Function Set-FolderAclRule
{
    [CmdletBinding()]
    PARAM($folder)

    #add folder permissions for EVERYONE account on site folder (development only)
    Log-Info "Begin - Adding folder permissions for $folder"
    TRY
    {
        $acl = Get-Acl $folder

        $rule = New-Object System.Security.AccessControl.FileSystemAccessRule("EVERYONE","FullControl", "ContainerInherit,ObjectInherit", "None", "Allow")
        $acl.AddAccessRule($rule)
        Set-Acl $folder $acl
    }
    CATCH
    {
        # NOT Critical Error
        $ErrorMessage = $_.Exception.Message
        Log-Error $ErrorMessage
    }

    Log-Info "End - Adding folder permissions"
}
